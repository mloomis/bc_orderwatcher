Order Guard Pipeline
====================

This pipeline allows regular checks of order generation on the production instance.
It searches for the most recently created orders, and will log an error or send an
electronic mail message if no order below the specified maximum age is found.

Documentation
-------------

See [description](documents/Description.md) for more information.

History
-------

- 2014/01/30 - Adjusted project structure to follow Demandware conventions.
- 2013/01/25 - Converted to GIT and pushed to bitbucket.
- 2012/10/18 - Created to address quota issue in existing order watcher.